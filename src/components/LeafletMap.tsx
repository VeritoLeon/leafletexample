import React, { Component } from "react";
import { Map, TileLayer } from "react-leaflet";
import { LatLngTuple } from 'leaflet';
import Routing from "./RoutingMachine";

export default class LeafletMap extends Component {
  map!: Map;
  state = {
    lat: 57.74,
    lng: 11.94,
    zoom: 13,
    isMapInit: false
  };
  saveMap = (map: Map) => {
    this.map = map;
    debugger;
    this.setState({
      isMapInit: true
    });
  };

  render() {
    const position: LatLngTuple = [this.state.lat, this.state.lng];
    console.log(this.map);
    return (
      <Map center={position} zoom={this.state.zoom} ref={this.saveMap}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        {this.state.isMapInit && <Routing map={this.map} />}
      </Map>
    );
  }
}
