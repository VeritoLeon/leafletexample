import L from "leaflet";
import "leaflet-routing-machine";
import { Map, MapLayer, MapLayerProps, withLeaflet } from "react-leaflet";

export interface RoutingProps extends MapLayerProps {
  map: Map;
}

class Routing extends MapLayer<RoutingProps> {
  createLeafletElement() {
    const { map } = this.props;
    let leafletElement = L.Routing.control({
      waypoints: [L.latLng(27.67, 85.316), L.latLng(27.68, 85.321)]
    }).addTo(map.leafletElement);
    return leafletElement.getPlan();
  }
}
export default withLeaflet(Routing);
